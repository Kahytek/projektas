﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projektas
{
    public partial class Countries : Form
    {
        //AddForm addForm = new AddForm();
        //List<CountryViewmodel> countriesList = new List<CountryViewmodel>();
        public Countries()
        {
            InitializeComponent();
            GridConfig();
            //foreach (string s in addForm.countriesList1)
            //{
            //    countriesList.Add(s);
            //}
        }
       

        private void GridConfig()
        {

            DataGridViewTextBoxColumn nameColumn = new DataGridViewTextBoxColumn
            {
                Name = "Name",
                HeaderText = "Name",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 120,
                Resizable = DataGridViewTriState.False
            };
            dgwCountries.Columns.Add(nameColumn);

            DataGridViewButtonColumn editButton = new DataGridViewButtonColumn
            {
                Name = "¨btnEdit",
                HeaderText = "Edit",
                UseColumnTextForButtonValue = true,
                Text = "Edit",
                CellTemplate = new DataGridViewButtonCell(),
                Width = 95,
                Resizable = DataGridViewTriState.False,
                SortMode = DataGridViewColumnSortMode.NotSortable
            };
            dgwCountries.Columns.Add(editButton);

            DataGridViewButtonColumn deleteButton = new DataGridViewButtonColumn
            {
                Name = "Delete",
                HeaderText = "Delete",
                UseColumnTextForButtonValue = true,
                Text = "Delete",
                CellTemplate = new DataGridViewButtonCell(),
                Width = 95,
                Resizable = DataGridViewTriState.False,
                SortMode = DataGridViewColumnSortMode.NotSortable
            };
            dgwCountries.Columns.Add(deleteButton);
        }

       // var bindingList = new BindingList<CountryViewmodel>(countriesList1);

        private void btnNew_Click(object sender, EventArgs e)
        {
            AddForm form = new AddForm();
            form.Show();
            this.Hide();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Edit editForm = new Edit();
            editForm.Show();
            this.Hide();
        }
    }
}
